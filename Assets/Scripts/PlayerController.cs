﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Character
{
    [SerializeField]
    private LayerMask layerGround;
    
    [SerializeField]
    private float speedPlayer = 1f;
    protected override void Start()
    {
        speed = speedPlayer;
        base.Start();
    }
    protected override void Update()
    {
        if (IsGrounded())
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            Vector3 dir = new Vector3(horizontal, 0f, vertical);
            if (horizontal != 0 || vertical != 0)
            {
                Move(dir);
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Stop();
        }
        base.Update();
    }
    private bool IsGrounded()
    {
        bool result = false;
        result = Physics.Raycast(transform.position, Vector3.down, 1f, layerGround);
        return result;
    }
}