﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    [SerializeField]
    private float playerHealthBase = 10f;
    [SerializeField]
    private int playerCoin = 0;
    [SerializeField]
    private float playerDamage = 1f;
    public Action<Creep> impactCreep;
    public Action<Coin> impactCoin;
    protected override void Start()
    {
        healthBase = playerHealthBase;
        healthCur = playerHealthBase;
        damage = playerDamage;
        coin = playerCoin;
        setDefaultValue();
        base.Start();
    }
    protected override void Update()
    {
        base.Update();   
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Creep"))
        {
            impactCreep(other.gameObject.GetComponent<Creep>());
        }
        if (other.gameObject.tag.Equals("Coin"))
        {
            impactCoin(other.gameObject.GetComponent<Coin>());
        }
    }
}
