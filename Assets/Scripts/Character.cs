﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Character : MonoBehaviour, IParameter
{
    public float healthBase { get; set; }
    public int coin { get; set; }
    public float speed { get; set; }
    public float healthCur { get; set; }
    public float damage { get; set; }
    private Rigidbody rb;
    private Vector3 curDir = Vector3.zero;
    public Action<float, float> updateHealthUI;
    public Action<int> updateCoinUI;
    public Action gameover;
    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    protected virtual void Update()
    {

    }
    protected void Move(Vector3 dir)
    {
        if (rb.velocity.magnitude >= speed)
        {
            rb.velocity = new Vector3(dir.x * speed, dir.y * speed, dir.z * speed);
        }
        else
        {
            rb.velocity += dir * speed * Time.deltaTime;
        }
    }
    protected void Stop()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
    }
    public void takeDamage(float val)
    {
        healthCur -= val;
        checkHealth();
    }
    public float giveDamage()
    {
        return damage;
    }
    public void collectCoin(int value)
    {
        coin = coin + value;
        if (this.gameObject.CompareTag("Player"))
        {
            updateCoinUI(coin);
        }
    }
    protected void checkHealth()
    {
        if (healthCur >= healthBase)
        {
            healthCur = healthBase;
            return;
        }
        if (healthCur <= 0f)
        {
            healthCur = 0f;
            Die();
        }
        if (this.gameObject.CompareTag("Player"))
        {
            updateHealthUI(healthCur, healthBase);
        }
    }
    protected void Die()
    {
        if (!this.gameObject.tag.Equals("Player"))
        {
            GetComponent<Collider>().enabled = false;
            StartCoroutine(waitSoundStop());
        }
        else
        {
            gameover();
        }
    }
    
    IEnumerator waitSoundStop()
    {
        yield return new WaitForSeconds(0.35f);
        Destroy(this.gameObject);
    }
    protected void setDefaultValue()
    {
        if (this.gameObject.CompareTag("Player"))
        {
            updateHealthUI(healthCur, healthBase);
            updateCoinUI(coin);
        }
    }
}