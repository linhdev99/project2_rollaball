﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creep : Character
{
    [SerializeField]
    private float creepHealthBase = 10f;
    [SerializeField]
    private float creepDamage = 1f;
    public AudioSource sfx;
    protected override void Start()
    {
        healthBase = creepHealthBase;
        healthCur = creepHealthBase;
        damage = creepDamage;
        base.Start();
    }
    protected override void Update()
    {
        base.Update();   
    }
    public void PlaySound()
    {
        sfx.Play();
    }
}
