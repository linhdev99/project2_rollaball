﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Player player;
    public UIManager uIManager;
    private GameObject[] listCoin;
    private int countCoin;
    void Awake()
    {
        player.impactCreep = impactCreep;
        player.impactCoin = impactCoin;
        player.updateHealthUI = updateHealthUI;
        player.updateCoinUI = updateCoinUI;
        player.gameover = gameover;
    }
    void Start()
    {
        listCoin = GameObject.FindGameObjectsWithTag("Coin");
        countCoin = listCoin.Length;
    }
    void Update()
    {

    }
    void impactCreep(Creep creep)
    {
        player.takeDamage(creep.giveDamage());
        impactPlayer(creep);
    }
    void impactPlayer(Creep creep)
    {
        creep.PlaySound();
        creep.takeDamage(player.giveDamage());
    }
    void impactCoin(Coin coin)
    {
        countCoin--;
        player.collectCoin(coin.value);
        coin.PlaySound();
        if (countCoin < 1)
        {
            gameover();
        }
    }
    void updateHealthUI(float healthCur, float healthBase)
    {
        string txt = "Health: " + healthCur.ToString() + "/" + healthBase.ToString();
        uIManager.updateHealth(txt);
    }
    void updateCoinUI(int coin)
    {
        string txt = "Coin: " + coin.ToString();
        uIManager.updateCoin(txt);
    }
    void gameover()
    {
        uIManager.GameOver();
    }
}
