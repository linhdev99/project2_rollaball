﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class RotateBehavior : MonoBehaviour
{
    public float speed = 10f;
    Rigidbody rb; 
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        rb.angularVelocity = new Vector3(0f, speed, 0f);
    }
}
