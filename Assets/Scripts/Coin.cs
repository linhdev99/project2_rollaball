﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int value = 1;
    public AudioSource collectSound;
    public void PlaySound()
    {
        collectSound.Play();
        GetComponent<Collider>().enabled = false;
        StartCoroutine(waitSoundStop());
    }
    IEnumerator waitSoundStop()
    {
        yield return new WaitForSeconds(0.35f);
        Destroy(this.gameObject);
    }
}
