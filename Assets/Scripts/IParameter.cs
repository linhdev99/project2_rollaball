﻿interface IParameter
{
    float healthBase {get; set;}
    float healthCur {get; set;}
    int coin {get; set;}
    float speed {get; set;}
    float damage {get; set;}
}
