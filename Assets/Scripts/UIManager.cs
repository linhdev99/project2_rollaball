﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text healthText;
    public Text coinText;
    public GameObject gameoverBG;
    public AudioSource soundGameover;
    public AudioSource soundBG;
    public void updateHealth(string txt)
    {
        healthText.text = txt;
    }
    public void updateCoin(string txt)
    {
        coinText.text = txt;
    }
    public void GameOver()
    {
        gameoverBG.SetActive(true);
        soundBG.Stop();
        soundGameover.Play();
    }
}
